#include <stdio.h>
#include <stdlib.h>
//#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <errno.h>

#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

#define DISK_BLOCK_SIZE 16
const unsigned long DISK_SIZE = 3 * 1024;
const char* DISK_FILE = "./mini.fs";

#define N_INODES 32
#define BLOCKS_PER_INODE 7
#define ROOT_INODE 1

#define FTYPE_FILE 1
#define FTYPE_DIR 2

#define MODE_TOUCH 1
#define MODE_OVWR 2


struct super_block {
    unsigned int block_size;
    unsigned int n_inodes;
    unsigned char* used_inodes;
    unsigned int n_blocks;
    unsigned char* used_blocks;
};

struct i_node {
    unsigned long i;
    unsigned char type;
    unsigned long size;
    unsigned long blocks[BLOCKS_PER_INODE];
};

struct leaf_info {
    unsigned long inode;
    char* name;
};

unsigned long sizeof_info(struct super_block *info);
struct super_block read_superblock(FILE *fp);
struct i_node read_inode(FILE *fp, struct super_block *info, unsigned long inode_i);
int touch_fs();
unsigned long ceil_div(unsigned long x, unsigned long y);
void write_inode_raw(FILE *fp, struct super_block *info, struct i_node *node);
unsigned long offset_inode0(struct super_block *info);
unsigned long offset_inode(struct super_block *info, unsigned long node_i);
unsigned long offset_block(unsigned long block_i, struct super_block *info);
unsigned long offset_block0(struct super_block *info);
unsigned long find_leaf_by_path(FILE *fp, struct super_block* info, char *path, unsigned long* parent_inode);
void write_superblock(FILE *fp, struct super_block *info);
unsigned long write_file(FILE *fp, struct super_block *info, char *path, char *data, unsigned long len, unsigned char file_type);
unsigned long write_file0(FILE *fp, struct super_block *info, char *file_name, unsigned long parent_inode, char *data,
                          unsigned long len, unsigned char type, int mode);
void write_block_raw(FILE *fp, struct super_block *info, unsigned long block_i, void* data, size_t len);
unsigned long find_free_blocks(const unsigned char *where, unsigned long len, unsigned long from, unsigned long n, unsigned long *result);
unsigned int read_block(FILE *fp, struct super_block *info, unsigned long block_i, void* out);
unsigned long write_root(FILE *fp, struct super_block *info);
unsigned long read_file(FILE *fp, struct super_block *info, char *path, char *out_buf, int* err);
struct leaf_info extract_leaf_info(FILE *fp, struct super_block *info, unsigned long block_i);
int rm_file0(FILE *fp, struct super_block *info, struct i_node* parent, struct i_node* leaf);
unsigned long
extract_leafs(FILE *fp, struct super_block *info, struct i_node* parent, struct leaf_info* leaf_infos);
int rm_file(FILE *fp, struct super_block *info, char *path);
void empty_block(FILE *fp, struct super_block *info, unsigned long block_i);
void write_leaf_info(FILE *fp, struct super_block *info, unsigned long block_i, struct leaf_info *leaf_info);
void unbuffer(FILE *pFile);

int main(int argc, char *argv[] )  {
    if (touch_fs()) {
        printf("Couldn't create the filesystem!\n");
        return 1;
    }
    if( argc >= 2 ) {
        FILE * fp = fopen(DISK_FILE, "rb+");
        if (fp == NULL) {
            printf("I/O or perms error while opening %s : errno=%d.\n", DISK_FILE, errno);
            return 1;
        }
        unbuffer(fp);
        struct super_block info = read_superblock(fp);

        int err = -1;
        char *cmd = argv[1];

        char *arg2 = NULL;
        if (argc >= 3) arg2 = argv[2];

        char *arg3 = NULL;
        if (argc >= 4) arg3 = argv[3];

        int found = 1;
        if (strcmp(cmd, "mkdir") == 0) {
            if (arg2 != NULL) {
                err = write_file(fp, &info, arg2, "", 0, FTYPE_DIR) <= 0;
                if (err)
                    printf("Couldn't create dir %s.\n", arg2);
                else
                    printf("Created dir %s.\n", arg2);
            } else
                printf("Usage: mkdir <path>\n");
        } else if (strcmp(cmd, "touch") == 0) {
            if (arg2 != NULL) {
                err = write_file(fp, &info, arg2, "", 0, FTYPE_FILE) <= 0;
                if (err)
                    printf("Couldn't create file %s.\n", arg2);
                else
                    printf("Created empty file %s.\n", arg2);
            } else
                printf("Usage: touch <path>\n");
        } else if (strcmp(cmd, "cat") == 0) {
            if (arg2 != NULL) {
                char out_buf[BLOCKS_PER_INODE * info.block_size];
                unsigned long len = read_file(fp, &info, arg2, out_buf, &err);
                if (!err) {
                    printf("--file %s : %lu bytes--\n", arg2, len);
                    fwrite(out_buf, sizeof(char), len, stdout);
                    printf("\n--end of file %s--\n", arg2);
                } else {
                    printf("Couldn't cat file %s!\n", arg2);
                }
            } else
                printf("Usage: cat <path>\n");
        } else if (strcmp(cmd, "extract") == 0) {
            if (arg2 != NULL && arg3 != NULL) {
                char content_buf[BLOCKS_PER_INODE * info.block_size];
                unsigned long len = read_file(fp, &info, arg2, content_buf, &err);
                FILE * target = fopen(arg3, "wb+");
                if (target != NULL) {
                    if (!err) {
                        err = fwrite(content_buf, sizeof(char), len, target) == 0;
                    }
                    if (err)
                        printf("Couldn't extract %s -> %s, probably i/o error!\n", arg2, arg3);
                    else
                        printf("Extracted %s -> %s.\n", arg2, arg3);
                    fclose(target);
                } else {
                    printf("Target %s is not writable!\n", arg3);
                }
            } else
                printf("Usage: extract <src> <target>\n");
        }  else if (strcmp(cmd, "write") == 0) {
            if (arg2 != NULL && arg3 != NULL) {
                FILE * src = fopen(arg2, "rb");

                unsigned int max_file_size = BLOCKS_PER_INODE * info.block_size;
                char content_buf[max_file_size];
                unsigned long len = fread(content_buf, sizeof(char), max_file_size, src);
                if (src != NULL) {
                    err = write_file(fp, &info, arg3, content_buf, len, FTYPE_FILE) <= 0;
                    if (err)
                        printf("Couldn't write %s -> %s, probably i/o error!\n", arg2, arg3);
                    else
                        printf("Written %s -> %s.\n", arg2, arg3);
                    fclose(src);
                } else {
                    printf("Target %s is not writable!\n", arg3);
                }
            } else
                printf("Usage: write <src> <target>\n");
        } else if (strcmp(cmd, "rm") == 0) {
            if (arg2 != NULL) {
                err = rm_file(fp, &info, arg2);
                if (err)
                    printf("Couldn't remove entry %s.\n", arg2);
                else
                    printf("Removed entry %s.\n", arg2);
            } else
                printf("Usage: rm <path>. Warning: recursive.\n");
        } else if (strcmp(cmd, "ls") == 0) {
            if (arg2 != NULL) {
                unsigned long file_i = find_leaf_by_path(fp, &info, arg2, NULL);
                if (file_i > 0) {
                    struct i_node file = read_inode(fp, &info, file_i);
                    if (file.type == FTYPE_DIR) {
                        struct leaf_info* leaf_infos = calloc(BLOCKS_PER_INODE, sizeof(struct leaf_info));
                        unsigned long n_leafs = extract_leafs(fp, &info, &file, leaf_infos);
                        printf("name\tinode\n");
                        for (int i=0; i < n_leafs; i++) {
                            struct leaf_info leafInfo = leaf_infos[i];
                            printf("%s\t%lu\n", leafInfo.name, leafInfo.inode);
                        }
                    } else {
                        printf("%s: is a file, inode=%lu\n", arg2, file.i);
                    }
                } else {
                    printf("%s: no such file or dir.\n", arg2);
                }
            } else
                printf("Usage: ls <path>.\n");
        } else {
            found = 0;
        }
        fclose(fp);
        if (found)
            return err;
    }
    printf("Unknown command format. Commands: mkdir, rm, touch, write, extract, cat, ls. Help is inside.\n");
    return -1;
}

int rm_file(FILE *fp, struct super_block* info, char *path) {
    unsigned long parent_i = 0;
    unsigned long leaf_i = find_leaf_by_path(fp, info, path, &parent_i);
    if (parent_i > 0 && leaf_i > 0) {
        struct i_node parent_inode = read_inode(fp, info, parent_i);
        struct i_node leaf_inode = read_inode(fp, info, leaf_i);
        return rm_file0(fp, info, &parent_inode, &leaf_inode);
    }
    return -1;
}

int touch_fs() {
    int fd = open(DISK_FILE, O_CREAT | O_RDWR | O_EXCL, 0666);
    if (fd < 0) {
        if (errno == EEXIST) {
            printf("[D] Working with existing fs %s.\n", DISK_FILE);
            return 0;
        }
        printf("[!] Couldn't open the file: %d.", errno);
        return 1;
    }

    printf("[*] Initial run, creating fs[size=%lu] at %s.\n", DISK_SIZE, DISK_FILE);

    FILE * fp = fdopen(fd, "wb+");
    unbuffer(fp);

    struct super_block *info = calloc(1, sizeof(struct super_block));
    info->block_size = DISK_BLOCK_SIZE * 2;
    info->n_inodes = N_INODES;
    info->used_inodes = calloc(info->n_inodes, sizeof(unsigned char));

    unsigned long first_block = offset_block0(info);
    info->n_blocks = (DISK_SIZE - first_block) / (info->block_size + 1); // +1 because of variable used_blocks[N_BLOCKS]
    if (first_block > DISK_SIZE || info->n_blocks <= 0) {
        printf("[!] low disk size: first_block at %lu, DISK_SIZE=%lu.\n", first_block, DISK_SIZE);
        return 1;
    }
    info->used_blocks = calloc(info->n_blocks, sizeof(unsigned char));
    assert(offset_block(info->n_blocks+1, info) <= DISK_SIZE);

    ftruncate(fd, 0);
    ftruncate(fd, DISK_SIZE);

    write_superblock(fp, info);
    write_root(fp, info);

    fclose(fp);
    printf("[*] Fs created!\n");
    return 0;
}

void unbuffer(FILE *pFile) {
    if (setvbuf(pFile, NULL, _IONBF, 0) != 0) {
        printf("[!] failed to set unbuffered io, exiting.\n");
        exit(1);
    }
}


unsigned long read_file(FILE *fp, struct super_block *info, char *path, char* out_buf, int* err) {
    *err = 0;
    if (path == NULL || strlen(path) == 0) {
        printf("[!] %s: invalid path.\n", path);
        *err = 1;
        return 0;
    }
    if (strcmp(&path[strlen(path)-1], "/") == 0) {
        printf("[!] %s: is a not a file (if it exists).\n", path);
        *err = 2;
        return 0;
    }

    unsigned long leaf_i = find_leaf_by_path(fp, info, path, NULL);
    if (leaf_i <= 0) {
        printf("[!] %s: no such file or directory.\n", path);
        *err = 4;
        return 0;
    }

    struct i_node leaf = read_inode(fp, info, leaf_i);
    if (leaf.type == FTYPE_FILE) {
        // extract file from blocks..
        unsigned long remaining = leaf.size;
        char buf[info->block_size];
        for (int i=0; i < BLOCKS_PER_INODE; i++) {
            unsigned long block_i = leaf.blocks[i];
            if (block_i > 0) {
                unsigned int read = read_block(fp, info, block_i, buf);
                unsigned long actual_len = min(info->block_size, remaining);
                strncpy(out_buf + i*info->block_size, buf, actual_len);
                remaining -= read;
            }
        }
        return leaf.size;
    } else {
        printf("[!] %s: is a directory.\n", path);
        *err = 3;
        return 0;
    }
}

unsigned long find_leaf_by_path(FILE *fp, struct super_block* info, char *path, unsigned long* parent_inode) {
    struct i_node cursor = read_inode(fp, info, ROOT_INODE);

    char *path_dup = strdup(path); // to allow nested strtok
    char *name = strtok(path_dup, "/");
    size_t c = 0;
    while (name != NULL) {
        int found = 0;
        if (cursor.type == FTYPE_DIR) {
            struct leaf_info* leaf_infos = calloc(BLOCKS_PER_INODE, sizeof(struct leaf_info));
            unsigned long n_leafs = extract_leafs(fp, info, &cursor, leaf_infos);
            for (int i=0; i < n_leafs; i++) {
                struct leaf_info leafInfo = leaf_infos[i];
                if (strcmp(name, leafInfo.name) == 0) {
                    if (parent_inode != NULL) *parent_inode = cursor.i;
                    cursor = read_inode(fp, info, leafInfo.inode);
                    found = 1;
                    break;
                }
            }
        }
        if (!found) {
            return 0;
        }
        c += strlen(name)+1;
        path_dup = strdup(path);
        name = strtok(path_dup + c, "/");
    }
    return cursor.i;
}

unsigned long write_file(FILE *fp, struct super_block *info, char *path, char *data, unsigned long len, unsigned char file_type) {
    // -- ensure parents are created
    unsigned long parent_inode = ROOT_INODE;
    char *prev_name = NULL;
    int is_root = (strcmp(path, "/") == 0);
    if (!is_root) {
        // todo: do I really need this shit?
        char tmp_path[strlen(path) + 1];
        tmp_path[strlen(path)] = '\0';
        strncpy(tmp_path, path, strlen(path));
        char *name = strtok(tmp_path, "/");
        while (name != NULL) {
            if (prev_name != NULL && strcmp(prev_name, "") != 0) { // if not last element in split(path, /) == if dir
                parent_inode = write_file0(fp, info, prev_name, parent_inode, "", 0, FTYPE_DIR, MODE_TOUCH);
                if (parent_inode <= 0) {
                    printf("[!] There was an error writing %s in %s!\n", prev_name, path);
                    return 0;
                }
            }
            prev_name = calloc(strlen(name)+1, sizeof(char));
            prev_name[strlen(name)] = '\0';
            strncpy(prev_name, name, strlen(name));
            name = strtok(NULL, "/");
        }
        // after that prev_name == file_name
    }

    return write_file0(fp, info, prev_name, parent_inode, data, len, file_type, MODE_OVWR);
}

unsigned long write_file0(FILE *fp, struct super_block *info,
                          char *file_name, unsigned long parent_inode,
                          char *data, unsigned long len, unsigned char file_type,
                          int mode) {
    if (file_name == NULL) {
        printf("[!] Oh no, won't rewrite root, go away.\n");
        return 0;
    }
//    printf("[D] writing %s to %lu!\n", file_name, parent_inode);

    if (file_type == FTYPE_DIR) {
        assert(strcmp(data, "") == 0);
        assert(len == 0);
    }

    unsigned long need_blocks_for_data = ceil_div(strlen(data) * sizeof(char), info->block_size);
    if (need_blocks_for_data > BLOCKS_PER_INODE) {
        printf("File is too large for this fs: %lu, max size=%u\n", len, BLOCKS_PER_INODE * info->block_size);
        return 0;
    }

    struct i_node parent = read_inode(fp, info, parent_inode);
    assert(parent.type == FTYPE_DIR);

    unsigned long overwrite_inode_gain = 0;
    unsigned long overwrite_blocks_gain = 0;
    unsigned long overwriting_inode = 0;

    // -- ensure parent dir has no like-named file with another type
//    printf("[D] Contents of %lu:\n", parent_inode);
    struct leaf_info* leaf_infos = calloc(BLOCKS_PER_INODE, sizeof(struct leaf_info));
    unsigned long n_leafs = extract_leafs(fp, info, &parent, leaf_infos);
    
    for (int i=0; i < n_leafs; i++) {
        struct leaf_info leafInfo = leaf_infos[i];
        if (strcmp(leafInfo.name, file_name) == 0) {
            struct i_node leaf = read_inode(fp, info, leafInfo.inode);
            if (leaf.type != file_type) {
                printf("[!] Dir %lu already has %s inside and its of type %d, not %d!\n", parent.i, file_name,
                       leaf.type, file_type);
                return 0;
            } else {
                if (leaf.type == FTYPE_FILE) {
                    printf("[*] found %s in %lu, will overwrite the leaf!\n", file_name, parent_inode);
                    // -- estimate gain from freeing the leaf resources
                    overwrite_blocks_gain = ceil_div(leaf.size, info->block_size);
                    overwrite_inode_gain = 1;
                    overwriting_inode = leaf.i;
                    break;
                } else {
                    printf("[!] Dir %s already exists in %lu, nop.\n", file_name, parent_inode);
                    return leaf.i;
                }
            }
        }
    }
//    printf("[D] /Contents of %lu\n", parent_inode);

    // -- ensure parent dir has one more slot in its inode for new leaf_ref block
    unsigned long parent_block_slot = 0;
    int found = 0;
    for (int i=0; i < BLOCKS_PER_INODE; i++) {
        if (parent.blocks[i] == 0) {
            parent_block_slot = i;
            found = 1;
            break;
        }
    }
    if (!found) {
        printf("[!] parent already has max children=BLOCKS_PER_INODE=%d!\n", BLOCKS_PER_INODE);
        return 0;
    }

    // -- find new block for child name
    unsigned long *leaf_ref_free_block_i = calloc(1, sizeof(unsigned long));
    unsigned long n_leaf_ref_free_blocks = find_free_blocks(info->used_blocks, info->n_blocks, 0, 1, leaf_ref_free_block_i);

    // -- create inode struct in hope that well find free blocks&inode
    struct i_node *node = calloc(1, sizeof(struct i_node));
    node->type = file_type;
    node->size = len;

    // -- find free blocks for data --
    unsigned long n_data_free_blocks = find_free_blocks(info->used_blocks, info->n_blocks, *leaf_ref_free_block_i, need_blocks_for_data, node->blocks);

    // -- ensure found enough free blocks
    unsigned long total_need_blocks = 1 + need_blocks_for_data; // leaf_ref + data
    unsigned long total_found_blocks = overwrite_blocks_gain + n_leaf_ref_free_blocks + n_data_free_blocks; // leaf_ref + data
    if (total_found_blocks < total_need_blocks) {
        printf("[!] Low disk space. Couldn't find %lu free blocks, found %lu.\n", total_need_blocks, total_found_blocks);
        return -2;
    }

    // -- find free inode --
    unsigned long *free_inode = calloc(1, sizeof(unsigned long));
    unsigned long n_free_inodes = find_free_blocks(info->used_inodes, info->n_inodes, 0, 1, free_inode);
    unsigned long total_need_inodes = 1; // new file
    unsigned long total_found_inodes = overwrite_inode_gain + n_free_inodes;
    if (total_found_inodes < total_need_inodes) {
        printf("[!] Couldn't find %lu free inodes, found %lu.\n", total_need_inodes, total_found_inodes);
        return 0;
    }
    node->i = *free_inode;

    // -- check i+filename will fit into block
    size_t max_filename_len = (info->block_size - sizeof(node->i)) / sizeof(char);
    size_t filename_len = strlen(file_name);
    if (filename_len > max_filename_len) {
        printf("[!] The filename is too long! Max len=%zu, your=%zu\n", max_filename_len, filename_len);
        return 0;
    }

    // -- only now, if sure that after rm it'll be possible to write file,
    // -- overwrite = rm + write
    if (overwriting_inode > 0) {
        struct i_node leaf = read_inode(fp, info, overwriting_inode);
        rm_file0(fp, info, &parent, &leaf);
        // I hate doing this, but more so I hate merging free overwritten blocks &
        // original free blocks for corner cases
        return write_file0(fp, info, file_name, parent_inode, data, len, file_type, mode);
    }

    // -- write content --
    for (int i=0; i < need_blocks_for_data; i++) {
        char* block_start = data + i*info->block_size;
        unsigned long block_len = len - i*info->block_size;
        block_len = min(block_len, info->block_size);
        write_block_raw(fp, info, node->blocks[i], block_start, block_len);
    }

    // -- write inode --
    write_inode_raw(fp, info, node);

    // -- write parent dir reference to child
    struct leaf_info *leaf_info = calloc(1, sizeof(struct leaf_info));
    leaf_info->name = file_name;
    leaf_info->inode = node->i;
    parent.blocks[parent_block_slot] = *leaf_ref_free_block_i;
    write_leaf_info(fp, info, *leaf_ref_free_block_i, leaf_info);
    write_inode_raw(fp, info, &parent);

//    printf("[D] --/written %s to %lu!--\n", file_name, parent_inode);

    return node->i;
}

void write_leaf_info(FILE *fp, struct super_block *info, unsigned long block_i, struct leaf_info *leaf_info) {
    // yay, copypasta!
    unsigned long offset = offset_block(block_i, info);
    fseek(fp, (long) offset, SEEK_SET);
    assert(sizeof(leaf_info->inode) + sizeof(char)*strlen(leaf_info->name) <= info->block_size);

    fwrite(&leaf_info->inode, sizeof(leaf_info->inode), 1, fp);
    fwrite(leaf_info->name, sizeof(char), strlen(leaf_info->name), fp);

    //todo : generalize & RM COPYPASTA!
    assert(block_i >= 1);
    info->used_blocks[block_i-1] = 1;
    write_superblock(fp, info); // sync!
}

unsigned long
extract_leafs(FILE *fp, struct super_block *info, struct i_node* parent, struct leaf_info* leaf_infos) {
    unsigned long c = 0;
    for (int i=0; i < BLOCKS_PER_INODE; i++) {
        unsigned long block_i = parent->blocks[i];
        if (block_i > 0) {
            struct leaf_info leafInfo = extract_leaf_info(fp, info, block_i);
            leaf_infos[c] = leafInfo;
            c++;
        }
    }
    return c;
}

int rm_file0(FILE *fp, struct super_block *info, struct i_node* parent, struct i_node* leaf) {
    if (leaf->i == ROOT_INODE) {
        printf("[!] Nope, wouldn't remove root dir!\n");
        return 1;
    }

    // -- find leaf internal id
    unsigned long parent_internal_leaf_i = 0;
    for (int i=0; i < BLOCKS_PER_INODE; i++) {
        unsigned long block_i = parent->blocks[i];
        if (block_i <= 0) {
            continue;
        }

        struct leaf_info leafInfo = extract_leaf_info(fp, info, block_i);

        if (leafInfo.inode == leaf->i) {
            parent_internal_leaf_i = i;
            break;
        }
    }
    unsigned long leaf_info_block_i = parent->blocks[parent_internal_leaf_i];

    // -- empty leaf contents
    for (int k=0; k<BLOCKS_PER_INODE; k++) {
        unsigned long leaf_data_block_i = leaf->blocks[k];
        if (leaf_data_block_i > 0) {
            if (leaf->type == FTYPE_FILE) {
                // just free leaf content
                empty_block(fp, info, leaf_data_block_i);
//                printf("[D] Emptying %luth data block from %lu..\n", leaf_data_block_i, leaf->i);
            } else {
                struct leaf_info leaf_of_leaf_info = extract_leaf_info(fp, info, leaf_data_block_i);
//                printf("[D] Recursively removing %s from %lu..\n", leaf_of_leaf_info.name, leaf->i);
                struct i_node leaf_of_leaf_inode = read_inode(fp, info, leaf_of_leaf_info.inode);
                rm_file0(fp, info, leaf, &leaf_of_leaf_inode);
            }
        }
    }
    // -- remove references of leaf
    info->used_inodes[leaf->i - 1] = 0; // free leaf inode, sync is done in empty_block
    empty_block(fp, info, leaf_info_block_i); // truncate leaf_info block
    parent->blocks[parent_internal_leaf_i] = 0; // free leaf_info reference in parent

    // -- save
    write_inode_raw(fp, info, parent);
    write_superblock(fp, info);

    return 0;
}

void empty_block(FILE *fp, struct super_block *info, unsigned long block_i) {
    char *empty_block = calloc(info->block_size, sizeof(char));
    write_block_raw(fp, info, block_i, empty_block, info->block_size); // truncate
    info->used_blocks[block_i - 1] = 0; // unuse
    write_superblock(fp, info); //sync!
}

struct leaf_info extract_leaf_info(FILE *fp, struct super_block *info, unsigned long block_i) {
    assert(block_i >= 1);
    assert(info->used_blocks[block_i-1]);

    struct leaf_info out;

    unsigned long offset = offset_block(block_i, info);
    fseek(fp, (long) offset, SEEK_SET);
    fread(&out.inode, sizeof(out.inode), 1, fp);
    size_t str_len = info->block_size - sizeof(out.inode);
    out.name = calloc(str_len, 1);
    fread(out.name, str_len, 1, fp);

    return out;
}

unsigned long write_root(FILE *fp, struct super_block *info) {
//    printf("[D] writing root!\n");
    struct i_node *node = calloc(1, sizeof(struct i_node));
    node->i = ROOT_INODE;
    node->type = FTYPE_DIR;
    node->size = 0;
    write_inode_raw(fp, info, node);
    return ROOT_INODE;
}

unsigned int read_block(FILE *fp, struct super_block *info, unsigned long block_i, void* out) {
    assert(block_i >= 1);
    assert(info->used_blocks[block_i-1]);

    unsigned long offset = offset_block(block_i, info);
    fseek(fp, (long) offset, SEEK_SET);
    return fread(out, 1, info->block_size, fp);
}

unsigned long find_free_blocks(const unsigned char *where, unsigned long len, unsigned long from, unsigned long n, unsigned long *result) {
    if (n<=0) {
        return 0;
    }
//    assert(from >= 1); // blocks & inodes are indexed from 1!
    unsigned long c = 0;
    for (unsigned long i=from; i < len; i++) {
        if (!where[i]) {
            if (result != NULL) {
                // blocks & inodes are indexed from 1 & stored in arrays in i-1 form.
                result[c] = i + 1;
            }
            c += 1;
            if (c >= n) {
                return c;
            }
        }
    }
    return c;
}

void write_block_raw(FILE *fp, struct super_block *info, unsigned long block_i, void* data, size_t len) {
    // guaranteed: 0 <= len <= FS_BLOCK_SIZE
    unsigned long offset = offset_block(block_i, info);
    fseek(fp, (long) offset, SEEK_SET);
    fwrite(data, len, 1, fp);

    assert(block_i >= 1);
    info->used_blocks[block_i-1] = 1;
    write_superblock(fp, info); // sync!
}

void write_superblock(FILE *fp, struct super_block *info) {
    fseek(fp, 0, SEEK_SET);

    fwrite(&info->block_size, sizeof info->block_size, 1, fp);

    fwrite(&info->n_inodes, sizeof info->n_inodes, 1, fp);
    fwrite(info->used_inodes, sizeof(unsigned char), info->n_inodes, fp);

    fwrite(&info->n_blocks, sizeof info->n_blocks, 1, fp);
    fwrite(info->used_blocks, sizeof(unsigned char), info->n_blocks, fp);
}

void write_inode_raw(FILE *fp, struct super_block *info, struct i_node *node) {
    unsigned long node_i = node->i;
    assert(node_i >= 1);
    unsigned long offset = offset_inode(info, node_i);
    fseek(fp, (long) offset, SEEK_SET);
    fwrite(node, sizeof(struct i_node), 1, fp);

    info->used_inodes[node_i-1] = 1;
    write_superblock(fp, info); // sync!
}

struct i_node read_inode(FILE *fp, struct super_block *info, unsigned long inode_i) {
    unsigned long offset = offset_inode(info, inode_i);
    struct i_node out;
    fseek(fp, (long) offset, SEEK_SET);
    fread(&out, sizeof(struct i_node), 1, fp);
    return out;
}

struct super_block read_superblock(FILE *fp) {
    struct super_block out;
    fseek(fp, 0, SEEK_SET);

    fread(&out.block_size, sizeof out.block_size, 1, fp);

    fread(&out.n_inodes, sizeof out.n_inodes, 1, fp);
    out.used_inodes = calloc(out.n_inodes, sizeof(unsigned char));
    fread(out.used_inodes, sizeof(unsigned char), out.n_inodes, fp);

    fread(&out.n_blocks, sizeof out.n_blocks, 1, fp);
    out.used_blocks = calloc(out.n_blocks, sizeof(unsigned char));
    fread(out.used_blocks, sizeof(unsigned char), out.n_blocks, fp);

    return out;
}

unsigned long offset_inode(struct super_block *info, unsigned long node_i) {
    assert(node_i >= 1);
    return offset_inode0(info) + (node_i - 1) * sizeof(struct i_node);
}

unsigned long offset_inode0(struct super_block *info) {
    return sizeof_info(info);
}

unsigned long sizeof_info(struct super_block *info) {
    return sizeof(info->block_size) +
           sizeof(info->n_inodes) + info->n_inodes*sizeof(unsigned char) +
           sizeof(info->n_blocks) + info->n_blocks*sizeof(unsigned char);
}

unsigned long offset_block(unsigned long block_i, struct super_block *info) {
    assert(block_i >= 1);
    return offset_block0(info) + (block_i-1) * info->block_size;
}

unsigned long offset_block0(struct super_block *info) {
    return offset_inode(info, info->n_inodes+1);
}

unsigned long ceil_div(unsigned long x, unsigned long y) {
    return  (x + y - 1) / y;
}
